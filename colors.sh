#!/bin/sh

# Work
export COLOR_RED=$(tput setaf 1);
export COLOR_GREEN=$(tput setaf 2);
export COLOR_YELLOW=$(tput setaf 3);
export COLOR_CYAN=$(tput setaf 6);
export COLOR_RESET=$(tput sgr0);

# Do not work
export COLOR_BLACK=$(tput setaf 0);
export COLOR_BLUE=$(tput setaf 4);
export COLOR_MAGENTA=$(tput setaf 5);
export COLOR_WHITE=$(tput setaf 7);

# Work
export LOG="${COLOR_GREEN}[LOG] --- "
export DEBUG="${COLOR_CYAN}[DEBUG] --- "
export WARN="${COLOR_YELLOW}[WARN] --- "
export ERROR="${COLOR_RED}[ERROR] --- "
